#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTextDocumentWriter>
#include <QMessageBox>
#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_lineEdit_textChanged(const QString &arg1)
{
    ui->lineEdit_2->setText(arg1);
}

void MainWindow::on_lineEdit_2_textChanged(const QString &arg1)
{
    ui->lineEdit->setText(arg1);
}
